_addon.name = 'AutoTarget_addon'
_addon.author = 'Helmaru'
_addon.version = '1.2 Windower'
_addon.commands = {'attaddon', 'att'}

local port = 19770
local ip = "127.0.0.1"
local att_auto = true


local socket = require("socket")
require ('packets')
require('logger')


function sendMessage(msg)
	local CP_connect = assert(socket.udp())
	CP_connect:settimeout(1)
	assert(CP_connect:sendto(msg, ip, port))
	CP_connect:close()
end

function printHelp()
	log( "HELP:")
	log( "//attaddon gettarget -- to get closest target")
	log( "//attaddon auto -- toggles overwrite from autotarget")
end

windower.register_event('addon command', function(input, ...)
	local args = {...}
	if args ~= nil and input ~= nil then
		local cmd = string.lower(input)
		if cmd == "settings" then
			if args[1] and args[2] then
				ip = args[1]
				port = args[2]
				log( "NETWORK UPDATE:  IP address: " .. ip .. " / Port number: " .. port)
			end
		elseif cmd == "verify" then
			local CP_connect = assert(socket.udp())
			CP_connect:settimeout(1)
			assert(CP_connect:sendto("AUTOTARGET_confirmed", ip, port))
			CP_connect:close()
		elseif cmd == "gettarget" then
			log( "Manual-targeting - Searching")
			sendMessage("AUTOTARGET_gettarget")
		elseif cmd == "stop" then
			log( "STOPPING")
			sendMessage("AUTOTARGET_stop")
		elseif cmd == "start" then
			log( "STARTING")
			sendMessage("AUTOTARGET_start")
		elseif cmd == "followstart" then
			log( "START FOLLOWING "..args[1])
			sendMessage("AUTOTARGET_followstart_"..args[1])
		elseif cmd == "sticky" then
			log( "SETTING STICKY TO "..args[1])
			sendMessage("AUTOTARGET_sticky_"..args[1])
		elseif cmd == "followstop" then
			log( "STOP FOLLOWING")
			sendMessage("AUTOTARGET_followstop")
		elseif cmd == "find" then
			log( "FIND "..args[1])
			sendMessage("AUTOTARGET_find_"..args[1])
		elseif cmd == "auto" then
			att_auto = not att_auto
			log( "OVERWRITE SET TO: "..tostring(att_auto))
		elseif cmd == "turn" then
			facemob()
		elseif cmd == "turnto" then
			local x = table.remove(args,1)
			local y = table.remove(args,1)
			facedir(x, y, string.lower(table.concat(args,' ')))
		elseif cmd == "echo" then
			if args[1] then
				local message = table.concat(args, " ")
				log( message)
			end
		else
			printHelp()
		end
	else
		printHelp()
	end
end)

function facemob()
	--log( "Turning")
	local target = windower.ffxi.get_mob_by_index(windower.ffxi.get_player().target_index or 0)
	if target == nil then return end
	local self_vector = windower.ffxi.get_mob_by_index(windower.ffxi.get_player().index or 0)
	local angle = (math.atan2((target.y - self_vector.y), (target.x - self_vector.x))*180/math.pi)*-1
	windower.ffxi.turn((angle):radian())
end
function facedir(x, y, name)
	log( 'Turning to ['..name..'] x: '..x..' y: '..y..'')
	local self_vector = windower.ffxi.get_mob_by_index(windower.ffxi.get_player().index or 0)
	local angle = (math.atan2((tonumber(y) - self_vector.y), (tonumber(x) - self_vector.x))*180/math.pi)*-1
	windower.ffxi.turn((angle):radian())
end


windower.register_event('incoming text',function(org)     
	if not att_auto then return end

	if string.find(string.lower(org), "targeting the ") then
		--print(org)
		log( "Auto-targeting - Overwriting")
		sendMessage("AUTOTARGET_gettarget")
	end
end)