﻿using EliteMMO.API;

namespace TargetAcquired
{
    partial class TargetAcquiredForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TargetAcquiredForm));
            this.POLID2 = new System.Windows.Forms.ComboBox();
            this.monitoredLabel = new System.Windows.Forms.Label();
            this.plLabel = new System.Windows.Forms.Label();
            this.charselect = new System.Windows.Forms.GroupBox();
            this.POLID = new System.Windows.Forms.ComboBox();
            this.setinstance = new System.Windows.Forms.Button();
            this.setinstance2 = new System.Windows.Forms.Button();
            this.pauseButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.currentAction = new System.Windows.Forms.TextBox();
            this.assistCheckBox = new System.Windows.Forms.CheckBox();
            this.engageCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.stickyLockCheckBox = new System.Windows.Forms.CheckBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.engageLockCheckBox = new System.Windows.Forms.CheckBox();
            this.assistLockCheckBox = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.followLockCheckBox = new System.Windows.Forms.CheckBox();
            this.followCheckBox = new System.Windows.Forms.CheckBox();
            this.stickyCheckBox = new System.Windows.Forms.CheckBox();
            this.charselect.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // POLID2
            // 
            this.POLID2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.POLID2.FormattingEnabled = true;
            this.POLID2.Location = new System.Drawing.Point(153, 34);
            this.POLID2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.POLID2.Name = "POLID2";
            this.POLID2.Size = new System.Drawing.Size(101, 21);
            this.POLID2.TabIndex = 3;
            // 
            // monitoredLabel
            // 
            this.monitoredLabel.AutoSize = true;
            this.monitoredLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.monitoredLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.monitoredLabel.Location = new System.Drawing.Point(3, 38);
            this.monitoredLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.monitoredLabel.Name = "monitoredLabel";
            this.monitoredLabel.Size = new System.Drawing.Size(114, 13);
            this.monitoredLabel.TabIndex = 5;
            this.monitoredLabel.Text = "Monitored Player: -";
            // 
            // plLabel
            // 
            this.plLabel.AutoSize = true;
            this.plLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.plLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.plLabel.Location = new System.Drawing.Point(4, 15);
            this.plLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.plLabel.Name = "plLabel";
            this.plLabel.Size = new System.Drawing.Size(88, 13);
            this.plLabel.TabIndex = 5;
            this.plLabel.Text = "Selected PL: -";
            // 
            // charselect
            // 
            this.charselect.BackColor = System.Drawing.Color.Transparent;
            this.charselect.Controls.Add(this.POLID2);
            this.charselect.Controls.Add(this.monitoredLabel);
            this.charselect.Controls.Add(this.plLabel);
            this.charselect.Controls.Add(this.POLID);
            this.charselect.Controls.Add(this.setinstance);
            this.charselect.Controls.Add(this.setinstance2);
            this.charselect.ForeColor = System.Drawing.SystemColors.GrayText;
            this.charselect.Location = new System.Drawing.Point(13, 14);
            this.charselect.Name = "charselect";
            this.charselect.Size = new System.Drawing.Size(334, 61);
            this.charselect.TabIndex = 18;
            this.charselect.TabStop = false;
            this.charselect.Text = " Character Selection ";
            // 
            // POLID
            // 
            this.POLID.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.POLID.FormattingEnabled = true;
            this.POLID.Location = new System.Drawing.Point(153, 10);
            this.POLID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.POLID.Name = "POLID";
            this.POLID.Size = new System.Drawing.Size(101, 21);
            this.POLID.TabIndex = 3;
            // 
            // setinstance
            // 
            this.setinstance.BackColor = System.Drawing.SystemColors.Menu;
            this.setinstance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.setinstance.ForeColor = System.Drawing.SystemColors.MenuText;
            this.setinstance.Location = new System.Drawing.Point(256, 9);
            this.setinstance.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.setinstance.Name = "setinstance";
            this.setinstance.Size = new System.Drawing.Size(73, 22);
            this.setinstance.TabIndex = 4;
            this.setinstance.Text = "Select";
            this.setinstance.UseVisualStyleBackColor = false;
            this.setinstance.Click += new System.EventHandler(this.setinstance_Click);
            // 
            // setinstance2
            // 
            this.setinstance2.BackColor = System.Drawing.SystemColors.Menu;
            this.setinstance2.Enabled = false;
            this.setinstance2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.setinstance2.ForeColor = System.Drawing.SystemColors.MenuText;
            this.setinstance2.Location = new System.Drawing.Point(256, 33);
            this.setinstance2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.setinstance2.Name = "setinstance2";
            this.setinstance2.Size = new System.Drawing.Size(73, 22);
            this.setinstance2.TabIndex = 4;
            this.setinstance2.Text = "Select";
            this.setinstance2.UseVisualStyleBackColor = false;
            this.setinstance2.Click += new System.EventHandler(this.setinstance2_Click);
            // 
            // pauseButton
            // 
            this.pauseButton.BackColor = System.Drawing.SystemColors.Menu;
            this.pauseButton.Enabled = false;
            this.pauseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pauseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Bold);
            this.pauseButton.ForeColor = System.Drawing.SystemColors.MenuText;
            this.pauseButton.Location = new System.Drawing.Point(352, 21);
            this.pauseButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(210, 47);
            this.pauseButton.TabIndex = 17;
            this.pauseButton.Text = "Pause";
            this.pauseButton.UseVisualStyleBackColor = false;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.currentAction);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.groupBox1.Location = new System.Drawing.Point(13, 173);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(546, 42);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Current Action";
            // 
            // currentAction
            // 
            this.currentAction.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.currentAction.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.currentAction.Location = new System.Drawing.Point(7, 19);
            this.currentAction.Name = "currentAction";
            this.currentAction.ReadOnly = true;
            this.currentAction.Size = new System.Drawing.Size(533, 13);
            this.currentAction.TabIndex = 0;
            // 
            // assistCheckBox
            // 
            this.assistCheckBox.AutoSize = true;
            this.assistCheckBox.Location = new System.Drawing.Point(35, 102);
            this.assistCheckBox.Name = "assistCheckBox";
            this.assistCheckBox.Size = new System.Drawing.Size(53, 17);
            this.assistCheckBox.TabIndex = 20;
            this.assistCheckBox.Text = "Assist";
            this.assistCheckBox.UseVisualStyleBackColor = true;
            this.assistCheckBox.CheckedChanged += new System.EventHandler(this.assistCheckBox_CheckedChanged);
            // 
            // engageCheckBox
            // 
            this.engageCheckBox.AutoSize = true;
            this.engageCheckBox.Location = new System.Drawing.Point(35, 135);
            this.engageCheckBox.Name = "engageCheckBox";
            this.engageCheckBox.Size = new System.Drawing.Size(63, 17);
            this.engageCheckBox.TabIndex = 21;
            this.engageCheckBox.Text = "Engage";
            this.engageCheckBox.UseVisualStyleBackColor = true;
            this.engageCheckBox.CheckedChanged += new System.EventHandler(this.engageCheckBox_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.stickyCheckBox);
            this.groupBox2.Controls.Add(this.stickyLockCheckBox);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Controls.Add(this.pictureBox4);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.engageLockCheckBox);
            this.groupBox2.Controls.Add(this.assistLockCheckBox);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.groupBox2.Location = new System.Drawing.Point(13, 76);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(546, 91);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Montored Options";
            // 
            // stickyLockCheckBox
            // 
            this.stickyLockCheckBox.AutoSize = true;
            this.stickyLockCheckBox.Location = new System.Drawing.Point(107, 60);
            this.stickyLockCheckBox.Name = "stickyLockCheckBox";
            this.stickyLockCheckBox.Size = new System.Drawing.Size(15, 14);
            this.stickyLockCheckBox.TabIndex = 30;
            this.stickyLockCheckBox.UseVisualStyleBackColor = true;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(106, 55);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(19, 21);
            this.pictureBox5.TabIndex = 29;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(106, 20);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(19, 21);
            this.pictureBox4.TabIndex = 28;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(-97, 35);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(19, 21);
            this.pictureBox2.TabIndex = 26;
            this.pictureBox2.TabStop = false;
            // 
            // engageLockCheckBox
            // 
            this.engageLockCheckBox.AutoSize = true;
            this.engageLockCheckBox.Location = new System.Drawing.Point(8, 60);
            this.engageLockCheckBox.Name = "engageLockCheckBox";
            this.engageLockCheckBox.Size = new System.Drawing.Size(15, 14);
            this.engageLockCheckBox.TabIndex = 24;
            this.engageLockCheckBox.UseVisualStyleBackColor = true;
            // 
            // assistLockCheckBox
            // 
            this.assistLockCheckBox.AutoSize = true;
            this.assistLockCheckBox.Location = new System.Drawing.Point(8, 27);
            this.assistLockCheckBox.Name = "assistLockCheckBox";
            this.assistLockCheckBox.Size = new System.Drawing.Size(15, 14);
            this.assistLockCheckBox.TabIndex = 24;
            this.assistLockCheckBox.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(7, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(19, 21);
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(7, 53);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(19, 21);
            this.pictureBox3.TabIndex = 27;
            this.pictureBox3.TabStop = false;
            // 
            // followLockCheckBox
            // 
            this.followLockCheckBox.AutoSize = true;
            this.followLockCheckBox.Location = new System.Drawing.Point(120, 103);
            this.followLockCheckBox.Name = "followLockCheckBox";
            this.followLockCheckBox.Size = new System.Drawing.Size(15, 14);
            this.followLockCheckBox.TabIndex = 22;
            this.followLockCheckBox.UseVisualStyleBackColor = true;
            this.followLockCheckBox.CheckedChanged += new System.EventHandler(this.followCheckBox_CheckedChanged);
            // 
            // followCheckBox
            // 
            this.followCheckBox.AutoSize = true;
            this.followCheckBox.Location = new System.Drawing.Point(134, 102);
            this.followCheckBox.Name = "followCheckBox";
            this.followCheckBox.Size = new System.Drawing.Size(56, 17);
            this.followCheckBox.TabIndex = 23;
            this.followCheckBox.Text = "Follow";
            this.followCheckBox.UseVisualStyleBackColor = true;
            // 
            // stickyCheckBox
            // 
            this.stickyCheckBox.AutoSize = true;
            this.stickyCheckBox.ForeColor = System.Drawing.Color.Black;
            this.stickyCheckBox.Location = new System.Drawing.Point(121, 59);
            this.stickyCheckBox.Name = "stickyCheckBox";
            this.stickyCheckBox.Size = new System.Drawing.Size(55, 17);
            this.stickyCheckBox.TabIndex = 31;
            this.stickyCheckBox.Text = "Sticky";
            this.stickyCheckBox.UseVisualStyleBackColor = true;
            // 
            // TargetAcquiredForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(571, 230);
            this.Controls.Add(this.followCheckBox);
            this.Controls.Add(this.followLockCheckBox);
            this.Controls.Add(this.engageCheckBox);
            this.Controls.Add(this.assistCheckBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.charselect);
            this.Controls.Add(this.pauseButton);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TargetAcquiredForm";
            this.Text = "TargetAcquired";
            this.charselect.ResumeLayout(false);
            this.charselect.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public Button setinstance;
        public Button setinstance2;
        public ComboBox POLID2;
        public Label monitoredLabel;
        public Label plLabel;
        public GroupBox charselect;
        public ComboBox POLID;
        public Button pauseButton;
        public GroupBox groupBox1;
        public TextBox currentAction;
        private CheckBox assistCheckBox;
        private CheckBox engageCheckBox;
        public GroupBox groupBox2;
        public CheckBox followLockCheckBox;
        public CheckBox followCheckBox;
        public CheckBox engageLockCheckBox;
        public CheckBox assistLockCheckBox;
        private PictureBox pictureBox1;
        private PictureBox pictureBox2;
        private PictureBox pictureBox3;
        private PictureBox pictureBox4;
        public CheckBox stickyLockCheckBox;
        private PictureBox pictureBox5;
        public CheckBox stickyCheckBox;
    }
}