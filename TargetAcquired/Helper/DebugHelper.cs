﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TargetAcquired.Helper
{
    public class DebugHelper
    {
        private TargetAcquiredForm _Form;
        public DebugHelper(TargetAcquiredForm form)
        {
            _Form = form;   
        }

        delegate void SetTextCallback(string text);

        public void AddAction(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (_Form.currentAction.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(AddAction);
                _Form.Invoke(d, new object[] { text });
            }
            else
            {
                _Form.currentAction.Text = text;
            }
        }

    }
}
