﻿using EliteMMO.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timer = System.Timers.Timer;

namespace TargetAcquired.Helper
{

    public enum PlayerStatus : byte
    {
        Standing = 0,
        Fighting = 1,
        Dead1 = 2,
        Dead2 = 3,
        Event = 4,
        Chocobo = 5,
        Healing = 33,
        Synthing = 44,
        Sitting = 47,
        Fishing = 56,
        FishBite = 57,
        Obtained = 58,
        RodBreak = 59,
        LineBreak = 60,
        CatchMonster = 61,
        LostCatch = 62,
        Unknown
    }

    internal class AssistHelper
    {
        private static EliteAPI _ELITEAPIPL;
        private static EliteAPI _ELITEAPIMON;
        private bool _Assist = false;
        private bool _Sticky = false;
        private bool _Engage = false;
        private bool _Follow = false;
        private int _FollowId = 0;
        private string _FollowName = null;
        private Timer _Worker;
        private static AddonHelper _Addon;
        private TargetAcquiredForm _Form;
        private int Tick = 0;

        public AssistHelper(TargetAcquiredForm form, EliteAPI pl, EliteAPI mon, AddonHelper addon)
        {
            _ELITEAPIPL = pl;
            _ELITEAPIMON = mon;
            _Worker = new Timer();
            _Worker.Interval = 200;
            _Worker.Elapsed += this.DoWork;
            _Worker.Enabled = true;
            _Worker.AutoReset = false; // makes it fire only once
            _Addon = addon;
            _Form = form;
        }
        public void Assist(bool toggle)
        {
            _Assist = toggle;

        }

        public void Sticky(bool toggle)
        {
            _Sticky = toggle;

        }

        private async void DoWork(object sender, EventArgs e)
        {
            try
            {
                if (_ELITEAPIPL != null && _ELITEAPIMON != null && _ELITEAPIPL.Player.HP > 0 && _ELITEAPIMON.Player.HP > 0)
                {
                    if (_Follow)
                    {
                        Follow();
                    }
                    else if(_Engage)
                    {
                        if (_ELITEAPIMON.Player.Status == (int)PlayerStatus.Fighting)
                        {
                            if (Tick % 6 == 0)
                            {
                                Tick = 0;
                                _Addon.TurnToMob();
                            }
                            Tick++;
                            var monTarget = _ELITEAPIMON.Target.GetTargetInfo().TargetIndex;
                            var plTarget = _ELITEAPIPL.Target.GetTargetInfo().TargetIndex;
                            if (monTarget != plTarget)
                            {
                                if (_ELITEAPIPL.Player.Status == (int)PlayerStatus.Fighting)
                                {
                                    _ELITEAPIPL.ThirdParty.SendString("/attack off");
                                    await Task.Delay(TimeSpan.FromSeconds(0.1));
                                }
                                if (_Assist)
                                {
                                    _ELITEAPIPL.Target.SetTarget((int)monTarget);
                                    await Task.Delay(TimeSpan.FromSeconds(0.1));
                                }
                                if (_Engage && _ELITEAPIPL.Player.Status != (int)PlayerStatus.Fighting)
                                {
                                    _ELITEAPIPL.ThirdParty.SendString("/attack on");
                                    await Task.Delay(TimeSpan.FromSeconds(0.3));
                                    //turn towards mob
                                    _Addon.TurnToMob();
                                }
                            }
                            else if (plTarget != 0 && _Engage)
                            {
                                LockOn();

                                EliteAPI.XiEntity followTarget = _ELITEAPIPL.Entity.GetEntity((int)plTarget);

                                EliteAPI.XiEntity monitor = _ELITEAPIPL.Entity.GetEntity((int)_ELITEAPIMON.Player.TargetID);

                                var maxDistance = _ELITEAPIPL.Player.ModelSize + followTarget.ModelSize + 3;

                                if (maxDistance < 3)
                                {
                                    maxDistance = 3;
                                }

                                if (followTarget.Distance > maxDistance)
                                {
                                    _ELITEAPIPL.ThirdParty.KeyDown(EliteMMO.API.Keys.W);
                                }
                                else
                                {
                                    _ELITEAPIPL.ThirdParty.KeyUp(EliteMMO.API.Keys.W);
                                }
                                if (_Engage && _ELITEAPIPL.Player.Status != (int)PlayerStatus.Fighting)
                                {
                                    _ELITEAPIPL.ThirdParty.SendString("/attack on");
                                    await Task.Delay(TimeSpan.FromSeconds(0.3));
                                    _Addon.TurnToMob();
                                }
                            }
                            else
                            {
                                UnLockOn();
                                _ELITEAPIPL.ThirdParty.KeyUp(EliteMMO.API.Keys.W);
                            }
                        }
                        else if (_ELITEAPIPL.Player.Status == (int)PlayerStatus.Fighting && !_Sticky)
                        {
                            _ELITEAPIPL.Target.SetTarget(0);
                            _ELITEAPIPL.ThirdParty.SendString("/attack off");
                            _ELITEAPIPL.ThirdParty.KeyUp(EliteMMO.API.Keys.W);
                        }
                        else
                        {
                            _ELITEAPIPL.ThirdParty.KeyUp(EliteMMO.API.Keys.W);
                        }
                    }
                }
                else
                {
                    _ELITEAPIPL.ThirdParty.KeyUp(EliteMMO.API.Keys.W);
                }
            }
            catch(Exception ex)
            {
                _ELITEAPIPL.Target.SetTarget(0);
                _ELITEAPIPL.ThirdParty.SendString("/attack off");
                _ELITEAPIPL.ThirdParty.KeyUp(EliteMMO.API.Keys.W);

            }
            _Worker.Start();
        }

        private void Follow()
        {
            try
            {

                if (_ELITEAPIPL.Player.Status == (int)PlayerStatus.Fighting)
                {
                    _ELITEAPIPL.ThirdParty.SendString("/attack off");
                }
                if (_FollowId == 0)
                {
                    if (_FollowName == null || _FollowName?.ToLower() == _ELITEAPIMON?.Player?.Name?.ToLower())
                    {
                        _FollowId = (int)_ELITEAPIMON.Player.TargetID;
                    }
                    else
                    {
                        _FollowId = followID();
                    }
                    if (_FollowId == 0)
                    {
                        return;
                    }
                }

                EliteAPI.XiEntity followTarget = _ELITEAPIPL.Entity.GetEntity(_FollowId);
                if (followTarget.TargetID == 0)
                {
                    _FollowId = 0;
                }

                // IF THE CURRENT TARGET IS NOT THE FOLLOWERS TARGET ID THEN CHANGE THAT NOW
                if (_ELITEAPIPL.Target.GetTargetInfo().TargetIndex != followTarget.TargetID)
                {
                    _ELITEAPIPL.Target.SetTarget(0);
                    Thread.Sleep(TimeSpan.FromSeconds(0.1));
                    _ELITEAPIPL.Target.SetTarget((int)followTarget.TargetID);
                    return;
                }
                // IF THE TARGET IS CORRECT BUT YOU'RE NOT LOCKED ON THEN DO SO NOW
                else if (_ELITEAPIPL.Target.GetTargetInfo().TargetIndex == followTarget.TargetID && !_ELITEAPIPL.Target.GetTargetInfo().LockedOn)
                {
                    _ELITEAPIPL.ThirdParty.SendString("/lockon <t>");
                    return;
                }
                if (Math.Truncate(followTarget.Distance) >= 1)
                {
                    _ELITEAPIPL.ThirdParty.KeyDown(EliteMMO.API.Keys.W);
                }
                else
                {
                    _ELITEAPIPL.ThirdParty.KeyUp(EliteMMO.API.Keys.W);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private int followID()
        {
            for (int x = 0; x < 2048; x++)
            {
                EliteAPI.XiEntity entity = _ELITEAPIPL.Entity.GetEntity(x);

                if (entity.Name != null && entity.Name.ToLower().Equals(_FollowName.ToLower()))
                {
                    return Convert.ToInt32(entity.TargetID);
                }
            }
            return 0;
        }


        private void LockOn()
        {
            if (!_ELITEAPIPL.Target.GetTargetInfo().LockedOn)
            {
                _ELITEAPIPL.ThirdParty.SendString("/lockon");
            }
        }
        private void UnLockOn()
        {
            if (_ELITEAPIPL.Target.GetTargetInfo().LockedOn)
            {
                _ELITEAPIPL.ThirdParty.SendString("/lockon");
            }
        }
        public void Engage(bool state)
        {
            _Engage = state;
            if (!state)
            {
                _ELITEAPIPL.ThirdParty.KeyUp(EliteMMO.API.Keys.W);
            }
        }
        public void Follow(bool state, string name = null)
        {
            _Follow = state;
            _FollowName = name;
            _FollowId = 0;
            if (!state)
            {
                UnLockOn();
                _ELITEAPIPL.Target.SetTarget(0);
                _ELITEAPIPL.ThirdParty.KeyUp(EliteMMO.API.Keys.W);
            }
        }
    }
}
