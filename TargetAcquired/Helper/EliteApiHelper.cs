﻿using EliteMMO.API;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TargetAcquired.Helper
{
    internal class EliteApiHelper
    {
        TargetAcquiredForm _Form;
        public List<int> processids = new List<int>();
        public List<int> processids2 = new List<int>();

        public EliteApiHelper(TargetAcquiredForm form)
        {
            _Form = form;
        }
        public bool CheckForDLLFiles()
        {
            if (!File.Exists("eliteapi.dll") || !File.Exists("elitemmo.api.dll"))
            {
                return false;
            }
            return true;
        }
        public EliteAPI GetPlInstance()
        {
            if (!CheckForDLLFiles())
            {
                MessageBox.Show(
                    "Unable to locate EliteAPI.dll or EliteMMO.API.dll\nMake sure both files are in the same directory as the application",
                    "Error");
                return null;
            }
            var ELITEAPIPL = new EliteAPI(processids[_Form.POLID.SelectedIndex]);
            _Form.plLabel.Text = "Selected PL: " + ELITEAPIPL.Player.Name;
            _Form.Text = _Form._AppName + " [" + ELITEAPIPL.Player.Name + "] - " + Application.ProductVersion;

            _Form.plLabel.ForeColor = Color.Green;
            _Form.POLID.BackColor = Color.White;
            _Form.setinstance2.Enabled = true;

            return ELITEAPIPL;
        }

        public EliteAPI GetMonInstance()
        {
            if (!CheckForDLLFiles())
            {
                MessageBox.Show(
                    "Unable to locate EliteAPI.dll or EliteMMO.API.dll\nMake sure both files are in the same directory as the application",
                    "Error");
                return null;
            }
            var ELITEAPIMonitored = new EliteAPI(processids[_Form.POLID2.SelectedIndex]);
            _Form.monitoredLabel.Text = "Monitoring: " + ELITEAPIMonitored.Player.Name;
            _Form.monitoredLabel.ForeColor = Color.Green;
            _Form.POLID2.BackColor = Color.White;
            _Form.pauseButton.Enabled = true;
            return ELITEAPIMonitored;
        }

        public void GetProcessIds()
        {
            IEnumerable<Process> pol = Process.GetProcessesByName("pol").Union(Process.GetProcessesByName("xiloader")).Union(Process.GetProcessesByName("edenxi"));

            if (pol.Count() < 1)
            {
                MessageBox.Show("FFXI not found");
            }
            else
            {
                for (int i = 0; i < pol.Count(); i++)
                {
                    _Form.POLID.Items.Add(pol.ElementAt(i).MainWindowTitle);
                    _Form.POLID2.Items.Add(pol.ElementAt(i).MainWindowTitle);
                    processids.Add(pol.ElementAt(i).Id);
                    processids2.Add(pol.ElementAt(i).Id);
                }
                _Form.POLID.SelectedIndex = 0;
                _Form.POLID2.SelectedIndex = 0;
            }
        }
    }
}
