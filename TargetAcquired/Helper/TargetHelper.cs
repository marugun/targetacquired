﻿using EliteMMO.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TargetAcquired.Helper
{
    internal class TargetHelper
    {
        private static EliteAPI _ELITEAPIPL;
        private static AddonHelper _Addon;
        private TargetAcquiredForm _Form;
        private string _Prefix = "[Targeting]";
        public TargetHelper(TargetAcquiredForm form, EliteAPI pl, AddonHelper addon)
        {
            _ELITEAPIPL = pl;
            _Addon = addon;
            _Form = form;
        }

        public void SetClosestTarget()
        {
            var targets = GetValidTargetsByDistance();
            if (targets.Count > 0)
            {
                var closest = targets.FirstOrDefault();
                _Form._Debug.AddAction($"{_Prefix} Command recieved: Target: {closest.Name} - Distance {closest.Distance}");
                _Addon.Echo($"Target: {closest.Name} - Distance {closest.Distance}");
                _ELITEAPIPL.Target.SetTarget((int)closest.TargetID);
                Task.Delay(TimeSpan.FromSeconds(1));
                _ELITEAPIPL.ThirdParty.SendString("/attack <t>");
            }
            else
            {
                _Form._Debug.AddAction($"{_Prefix} No Active Targets!");
                _Addon.Echo($"No Active Targets!");
            }
        }

        private List<EliteAPI.XiEntity> GetValidTargetsByDistance()
        {
            var targets = new List<EliteAPI.XiEntity>();
            for (int x = 0; x < 2048; x++)
            {
                EliteAPI.XiEntity z = _ELITEAPIPL.Entity.GetEntity(x);
                if (!string.IsNullOrEmpty(z.Name))
                {
                    var debug = $"{z.Name} has claim from {z.ClaimID} Status {z.Status} Type {z.Type} Health {z.HealthPercent} Flags {z.SpawnFlags} Pet Owner {z.PetOwnerID} {z.PetIndex} distance {z.Distance}";
                    if (z.Distance < 30 /*in distance*/ && z.HealthPercent > 0/*not dead*/ && z.Status == 1 /*engaged*/ && z.SpawnFlags == 16 && z.PetOwnerID == 0 && !z.Name.Contains("'s"))
                    {
                        Console.WriteLine($"{z.Name} has claim from {z.ClaimID} Status {z.Status} Type {z.Type} Health {z.HealthPercent} Flags {z.SpawnFlags} Pet Owner {z.PetOwnerID} {z.PetIndex} distance {z.Distance}");
                        targets.Add(z);
                    }
                    if (z.Name == "Helmaru")
                    {
                        //debug
                        // Console.WriteLine($"{z.Name} has claim from {z.ClaimID} Status {z.Status} {z.HealthPercent}");
                    }

                }
            }
            return targets.OrderBy(o => o.Distance).ToList();
        }
        public List<EliteAPI.XiEntity> GetTargetByName(string name)
        {
            var targets = new List<EliteAPI.XiEntity>();
            for (int x = 0; x < 2048; x++)
            {
                EliteAPI.XiEntity z = _ELITEAPIPL.Entity.GetEntity(x);
                if (!string.IsNullOrEmpty(z.Name))
                {
                    if (z.Name.ToLower().Contains(name.ToLower()))
                    {
                        targets.Add(z);
                    }
                }
            }
            return targets.OrderBy(o => o.Distance).ToList();
        }
    }
}
