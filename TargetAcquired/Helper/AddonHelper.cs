﻿using EliteMMO.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Timer = System.Windows.Forms.Timer;

namespace TargetAcquired.Helper
{
    internal class AddonHelper
    {
        private static EliteAPI _ELITEAPIPL;
        private TargetHelper _Target;
        private TargetAcquiredForm _Form;
        private BackgroundWorker _Worker;
        public AddonHelper(TargetAcquiredForm form, EliteAPI pl)
        {
            _ELITEAPIPL = pl;
            _Form = form;
            _Target = new TargetHelper(form, _ELITEAPIPL, this);
            _ELITEAPIPL.ThirdParty.SendString("//lua r AutoTarget_addon");
            Task.Delay(TimeSpan.FromSeconds(1));
            _Worker = new BackgroundWorker();
            _Worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ReadAddon);
            _Worker.RunWorkerAsync();
        }
        public void TurnToMob()
        {
            _ELITEAPIPL.ThirdParty.SendString(_Form._Prefix + " turn");
        }

        private Int32 GetFreePort()
        {
            
            var startingAtPort = 20770;
            var maxNumberOfPortsToCheck = 500;
            var range = Enumerable.Range(startingAtPort, maxNumberOfPortsToCheck);
            var portsInUse =
                from p in range
                join used in System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().GetActiveUdpListeners()
                on p equals used.Port
                select p;

            return range.Except(portsInUse).FirstOrDefault();
        }
        private void ReadAddon(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (_ELITEAPIPL != null)
            {
                var ip = "127.0.0.1";
                var freePort = GetFreePort();

                _ELITEAPIPL.ThirdParty.SendString(_Form._Prefix + " settings " + ip + " " + freePort);
                UdpClient listener = new UdpClient(freePort);
                IPEndPoint groupEP = new IPEndPoint(IPAddress.Parse(ip), freePort);
                string received_data;
                byte[] receive_byte_array;
                while (true)
                {
                    try
                    {
                        receive_byte_array = listener.Receive(ref groupEP);
                        received_data = Encoding.ASCII.GetString(receive_byte_array, 0, receive_byte_array.Length);
                        string[] commands = received_data.Split('_');

                        _Form._Debug.AddAction("[Addon] Command recieved: "+commands[1]);
                        if (commands[1] == "gettarget")
                        {
                            _Target.SetClosestTarget();
                        }
                        if (commands[1] == "stop")
                        {
                            if (!_Form.engageLockCheckBox.Checked)
                            {
                                _Form.Engage(false);
                            }
                        }
                        if (commands[1] == "start")
                        {
                            if (!_Form.engageLockCheckBox.Checked)
                            {
                                _Form.Engage(true);
                            }
                        }
                        if (commands[1] == "followstart")
                        {
                            if (!_Form.followLockCheckBox.Checked)
                            {
                                _Form.Follow(true, commands[2]);
                            }
                        }
                        if (commands[1] == "followstop")
                        {
                            if (!_Form.followLockCheckBox.Checked)
                            {
                                _Form.Follow(false, null);
                            }
                        }
                        if (commands[1] == "sticky")
                        {
                            if (!_Form.stickyLockCheckBox.Checked)
                            {
                                _Form.Sticky(bool.Parse(commands[2]));
                            }
                        }
                        if (commands[1] == "find")
                        {
                            var targets = _Target.GetTargetByName(commands[2]);
                            foreach(var target in targets) {
                                _ELITEAPIPL.ThirdParty.SendString(_Form._Prefix + $" turnto {target.X} {target.Y} Id: {target.TargetID} Name: {target.Name} - Distance: {target.Distance} - Status: {target.Status} - Type: {target.Type} - Flags: {target.SpawnFlags}");
                                Thread.Sleep(TimeSpan.FromSeconds(0.5));
                            }
                           //if(targets != null)
                           //{
                           //    _ELITEAPIPL.ThirdParty.SendString(_Form._Prefix + " turnto "+targets.X+" "+targets.Y+" " + targets.Distance + " " + targets.Name);
                           //}

                        }
                        Thread.Sleep(TimeSpan.FromSeconds(0.3));
                    }
                    catch (Exception error1)
                    {
                        Console.WriteLine(error1.ToString());
                    }
                }
                listener.Close();
            }
        }

        public void Echo(string str)
        {
            _ELITEAPIPL.ThirdParty.SendString(_Form._Prefix + "echo " + str);
        }

    }
}
