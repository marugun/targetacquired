using EliteMMO.API;
using TargetAcquired.Helper;

namespace TargetAcquired
{
    public partial class TargetAcquiredForm : Form
    {
        public EliteAPI _ELITEAPIPL;
        public EliteAPI _ELITEAPIMON;
        public string _Prefix = "//attaddon ";
        public string _AppName = "TargetAquired";
        private EliteApiHelper _Elite;
        public DebugHelper _Debug;
        private AddonHelper _Addon;
        private AssistHelper _Assist;
        
        public bool _Paused = false;
        public TargetAcquiredForm()
        {
            InitializeComponent();
            _Elite = new EliteApiHelper(this);
            _Debug = new DebugHelper(this);
            _Elite.GetProcessIds();
            Text = _AppName + " - " + Application.ProductVersion;
            Application.ApplicationExit += new EventHandler(OnApplicationExit);
        }

        private void setinstance_Click(object sender, EventArgs e)
        {
            _ELITEAPIPL = _Elite.GetPlInstance();
            _Addon = new AddonHelper(this, _ELITEAPIPL);
        }
        private void setinstance2_Click(object sender, EventArgs e)
        {
            _ELITEAPIMON = _Elite.GetMonInstance();
            _Assist = new AssistHelper(this, _ELITEAPIPL, _ELITEAPIMON, _Addon);

            engageCheckBox.Checked = true;
            assistCheckBox.Checked = true;
            followCheckBox.Checked = true;
            Follow(followCheckBox.Checked);
            _Assist.Assist(assistCheckBox.Checked);
            _Assist.Engage(engageCheckBox.Checked);
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            if (_Paused)
            {
                pauseButton.Text = "Pause";
                pauseButton.ForeColor = Color.Black;
                _Paused = false;
               

            }
            else
            {
                pauseButton.Text = "Paused!";
                pauseButton.ForeColor = Color.Red;
                _Paused = true;
            }
            Engage(_Paused);
            Follow(_Paused);
        }

        public void Engage(bool engage)
        {
            _Assist.Engage(engage);
            if (engageCheckBox.InvokeRequired)
            {
                engageCheckBox.Invoke(new MethodInvoker(delegate { engageCheckBox.Checked = engage; }));
            }
        }

        public void Sticky(bool sticky)
        {
            _Assist.Sticky(sticky);
            if (stickyCheckBox.InvokeRequired)
            {
                stickyCheckBox.Invoke(new MethodInvoker(delegate { stickyCheckBox.Checked = sticky; }));
            }
        }

        public void Follow(bool follow, string name = null)
        {
            _Assist.Follow(follow, name);
            if (followCheckBox.InvokeRequired)
            {
                followCheckBox.Invoke(new MethodInvoker(delegate { followCheckBox.Checked = follow; }));
            }
        }

        void OnApplicationExit(object sender, EventArgs e)
        {
            if (_ELITEAPIPL != null)
            {
                _ELITEAPIPL.ThirdParty.SendString("//lua unload AutoTarget_addon");
            }
        }

        private void assistCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            _Assist.Assist(assistCheckBox.Checked);
        }

        private void engageCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            _Assist.Engage(engageCheckBox.Checked);
        }

        private void followCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            _Assist.Follow(followCheckBox.Checked);
        }
    }
}